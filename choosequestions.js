
const geschichte = [
  {
    question: "Wie heißt heute die antike Stadt Byzanz?",
    answers: ["Damaskus", "Kairo", "Agadir", "Istanbul"],
    correctAnswer: 3
  },
  {
    question: "Wann endete der Dreißigjährige Krieg?",
    answers: ["1618", "1629", "1648", "1688"],
    correctAnswer: 2
  }
];

const geografie = [
  {
    question: "Wo ist die Berufsschule?",
    answers: ["Eppendorf" , "Volksdorf", "Wilhelmsburg"],
    correctAnswer: 2
  },
  {
    question: "Durch welche Stadt verläuft der 0. Längengrad?",
    answers: ["Paris", "Rom", "New York", "London"],
    correctAnswer: 3
  }
];

const sport = [{
    question: "In welchem Jahr wurden die ersten Olympischen Spiele der Moderen veranstalltet?",
    answers: ["1896", "1900", "1904", "1908"],
    correctAnswer: 0
}, {
    question: "Wie schwer ist der Diskus der Frauen?",
    answers: ["750 g", "1 kg", "2,5 kg", "5 kg"],
    correctAnswer: 1
}]

function choosequestions(category) {
    if("Geschichte" === category) {
        return geschichte;
    } else if ("Geografie" === category) {
        return geografie;
    }  else if ("Sport" === category) {
        return sport;
    } 
    return [];
}

module.exports = choosequestions;