const fs = require('fs');
const players = require('./server.js');

const SCOREBOARD_FILE = '/database/scoreboard.json';

function writePlayersToFile(players, filename) {
  try {
    fs.writeFileSync(filename, JSON.stringify(players, null, 2));
  } catch (error) {
    console.error('Error writing scoreboard file:', error);
  }
}

function readPlayersFromFile(filename) {
  try {
    const data = fs.readFileSync(filename, 'utf8');
    return JSON.parse(data);
  } catch (error) {
    console.error('Error reading scoreboard file:', error);
    return [];
  }
}

function sortPlayersByScore(initplayers) {
  return initplayers.sort((a, b) => b.score - a.score);
}

function addPlayerToList(initplayers, name, score) {
  initplayers.push({ name, score });
}

function updateScoreboard(players) {
  console.log('Player:', players);
  let initplayers = readPlayersFromFile(SCOREBOARD_FILE);
  
  initplayers = sortPlayersByScore(initplayers);
  
  addPlayerToList(initplayers, players.name, players.score);
  
  initplayers = sortPlayersByScore(initplayers);

  writePlayersToFile(initplayers, SCOREBOARD_FILE);
}

module.exports = updateScoreboard;