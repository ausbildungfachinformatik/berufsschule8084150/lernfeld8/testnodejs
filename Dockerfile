FROM node:22-alpine3.20
WORKDIR /app
COPY package*.json .
RUN npm install --omit-dev
COPY . .
RUN rm -r tests
RUN rm README.md
RUN rm LICENSE
RUN npm prune --omit-dev
RUN mkdir /database
EXPOSE 3000
CMD ["node", "server.js"]