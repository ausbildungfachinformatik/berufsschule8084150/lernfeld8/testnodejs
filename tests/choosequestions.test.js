const choosequestions = require('../choosequestions'); 

describe('choosequestions function', () => {
  test('returns correct questions for Geschichte category', () => {
    const result = choosequestions('Geschichte');
    expect(result).toHaveLength(2);
    expect(result[0].question).toBe("Wie heißt heute die antike Stadt Byzanz?");
    expect(result[1].question).toBe("Wann endete der Dreißigjährige Krieg?");
  });

  test('returns correct questions for Geografie category', () => {
    const result = choosequestions('Geografie');
    expect(result).toHaveLength(2);
    expect(result[0].question).toBe("Wo ist die Berufsschule?");
    expect(result[1].question).toBe("Durch welche Stadt verläuft der 0. Längengrad?");
  });

  test('returns correct questions for Sport category', () => {
    const result = choosequestions('Sport');
    expect(result).toHaveLength(2);
    expect(result[0].question).toBe("In welchem Jahr wurden die ersten Olympischen Spiele der Moderen veranstalltet?");
    expect(result[1].question).toBe("Wie schwer ist der Diskus der Frauen?");
  });

  test('returns empty array for unknown category', () => {
    const result = choosequestions('Unknown');
    expect(result).toEqual([]);
  });

  test('questions have correct structure', () => {
    const result = choosequestions('Geschichte');
    result.forEach(question => {
      expect(question).toHaveProperty('question');
      expect(question).toHaveProperty('answers');
      expect(question).toHaveProperty('correctAnswer');
      expect(Array.isArray(question.answers)).toBe(true);
      expect(typeof question.correctAnswer).toBe('number');
    });
  });
});
