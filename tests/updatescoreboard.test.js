const fs = require('fs');
const updateScoreboard = require('../updatescoreboard.js'); 

// Mock fs module
jest.mock('fs');

// Mock players module
jest.mock('../server.js', () => ({
  // Add any necessary mock data here
}));

describe('Scoreboard Functions', () => {
  const SCOREBOARD_FILE = '/database/scoreboard.json';

  beforeEach(() => {
    // Clear all mocks before each test
    jest.clearAllMocks();
  });

  test('writePlayersToFile should write players to file', () => {
    const players = [{ name: 'Alice', score: 100 }];
    fs.writeFileSync.mockImplementation(() => {});

    updateScoreboard({ name: 'Alice', score: 100 });

    expect(fs.writeFileSync).toHaveBeenCalledWith(
      SCOREBOARD_FILE,
      JSON.stringify(players, null, 2)
    );
  });

  test('readPlayersFromFile should read players from file', () => {
    const mockData = JSON.stringify([{ name: 'Bob', score: 90 }]);
    fs.readFileSync.mockReturnValue(mockData);

    updateScoreboard({ name: 'Alice', score: 100 });

    expect(fs.readFileSync).toHaveBeenCalledWith(SCOREBOARD_FILE, 'utf8');
  });

  test('sortPlayersByScore should sort players by score in descending order', () => {
    const mockPlayers = [
      { name: 'Bob', score: 90 },
      { name: 'Alice', score: 100 },
      { name: 'Charlie', score: 95 },
    ];
    fs.readFileSync.mockReturnValue(JSON.stringify(mockPlayers));

    updateScoreboard({ name: 'David', score: 85 });

    const expectedSortedPlayers = [
      { name: 'Alice', score: 100 },
      { name: 'Charlie', score: 95 },
      { name: 'Bob', score: 90 },
      { name: 'David', score: 85 },
    ];

    expect(fs.writeFileSync).toHaveBeenCalledWith(
      SCOREBOARD_FILE,
      JSON.stringify(expectedSortedPlayers, null, 2)
    );
  });

  test('addPlayerToList should add a new player to the list', () => {
    const mockPlayers = [{ name: 'Alice', score: 100 }];
    fs.readFileSync.mockReturnValue(JSON.stringify(mockPlayers));

    updateScoreboard({ name: 'Bob', score: 90 });

    const expectedPlayers = [
      { name: 'Alice', score: 100 },
      { name: 'Bob', score: 90 },
    ];

    expect(fs.writeFileSync).toHaveBeenCalledWith(
      SCOREBOARD_FILE,
      JSON.stringify(expectedPlayers, null, 2)
    );
  });

  test('updateScoreboard should handle file read error', () => {
    fs.readFileSync.mockImplementation(() => {
      throw new Error('File read error');
    });

    const consoleSpy = jest.spyOn(console, 'error');
    updateScoreboard({ name: 'Alice', score: 100 });

    expect(consoleSpy).toHaveBeenCalledWith(
      'Error reading scoreboard file:',
      expect.any(Error)
    );
    expect(fs.writeFileSync).toHaveBeenCalledWith(
      SCOREBOARD_FILE,
      JSON.stringify([{ name: 'Alice', score: 100 }], null, 2)
    );
  });

  test('updateScoreboard should handle file write error', () => {
    fs.readFileSync.mockReturnValue('[]');
    fs.writeFileSync.mockImplementation(() => {
      throw new Error('File write error');
    });

    const consoleSpy = jest.spyOn(console, 'error');
    updateScoreboard({ name: 'Alice', score: 100 });

    expect(consoleSpy).toHaveBeenCalledWith(
      'Error writing scoreboard file:',
      expect.any(Error)
    );
  });
});