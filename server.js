const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
const path = require('path');
const fs = require('fs');

const chooseQuestions = require('./choosequestions');

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

app.use(express.static(path.join(__dirname, 'public')));

let serverInstance;
let questions;
let players = {};
module.exports = players;

function startServer() {
  serverInstance = server.listen(3000, () => {
    console.log('Server started on port 3000');
  });

  io.on('connection', (socket) => {
    console.log('A user connected');
    let currentQuestion = 0;

    socket.on('join', (playerName) => {
      players[socket.id] = {
        name: playerName,
        score: 0
      };
      io.emit('players', players);
    });

    socket.on('category', (category) => {
      questions = chooseQuestions(category);
    });

    socket.on('start', (player) => {
      if (currentQuestion < questions.length) {
        io.emit('question', questions[currentQuestion]);
        currentQuestion++;
      } else {
        io.emit('end');
      }
    });

    socket.on('answer', (selectedAnswer) => {
      const player = players[socket.id];
      if (questions[currentQuestion].correctAnswer === selectedAnswer) {
        player.score++;
      }
      io.emit('scores', players);
    });

    socket.on('reset', () => {
      io.emit('reset');
      players = {};
      console.log('Restarting server...');
      io.close(() => {
        startServer();
      });
    });

    socket.on('disconnect', () => {
      console.log('A user disconnected');
    });
  });
}

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.get('/admin', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'admin.html'));
});

app.get('/score', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'scoreboard.html'));
});

startServer();

function readScoreboard() {
  try {
    const data = fs.readFileSync('/database/scoreboard.json', 'utf8');
    return JSON.parse(data);
  } catch (error) {
    console.error('Error reading scoreboard file:', error);
    return [];
  }
}

function updateScoreboard() {
  const scoreboard = readScoreboard();
  io.emit('updateScoreboardList', scoreboard);
}

setInterval(updateScoreboard, 10000);