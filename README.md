```bash
docker pull registry.gitlab.com/ausbildungfachinformatik/berufsschule8084150/lernfeld8/testnodejs:main &&
docker run -p 3000:3000 -v /home/$USER/quizapp:/database registry.gitlab.com/ausbildungfachinformatik/berufsschule8084150/lernfeld8/testnodejs:main
```

- User under /
- Admin under /admin
- Scoreboard unter /score
- U will need an empty scoreboard.json under /database or your existing scores 